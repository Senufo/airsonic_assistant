#!/usr/bin/env python

import json
from pprint import pprint
import libsonic
import time
import m3u8

# Load settings
f = open('settings.json', 'r')
content = f.read()
f.close()

# Get default settings variable
settings = json.loads(content)

# génére un salt avec le module secrets
# import secrets
# salt = secrets.token_hex(10) 
# print('salt: {}'.format(salt))
# import hashlib
# token = settings['password'] + salt
# print('token: {}'.format(token))
# token = hashlib.md5(token.encode('utf-8')).hexdigest()
# print('md5sum: {}'.format(token))
# exit()
# We pass in the base url, the username, password, and port number
# Be sure to use https:// if this is an ssl connection!
conn = libsonic.Connection(settings['host'] , settings['user'] , 
    settings['password'] , port=settings['port'], apiVersion='1.15.0')
# Let's get 2 completely random songs
# songs = conn.getRandomSongs(size=2)
# We'll just pretty print the results we got to the terminal
# pprint(songs)
# musicFoders = conn.getMusicFolders()
# pprint(musicFoders)
# pprint(conn.getNowPlaying())
# pprint(conn.getPlaylists())
# pprint(conn.getPlaylist(2))
print(conn.hls(1278))
hls = conn.hls(1278)
print(type(hls))
# m3u8_obj = m3u8.load('http://videoserver.com/playlist.m3u8')  # this could also be an absolute filename
m3u8_obj = m3u8.loads(hls)
print(m3u8_obj.segments)
print(m3u8_obj.target_duration)

# if you already have the content as string, use

# m3u8_obj = m3u8.loads('#EXTM3U8 ... etc ... ')
# pprint(conn.getRandomSongs(size=10, genre='jazz', fromYear=None, toYear=None, musicFolderId=None))
pprint(conn.stream(12770, estimateContentLength=True).__dict__)
print(conn.stream(12770, estimateContentLength=True))
pprint(conn.stream(12770).fileno())
pprint(conn.stream(12770).seekable())
exit()
id = 1278
# pprint(conn.getSong(id))
playlist = [301,220,5920]
for id in playlist:
    print('Début {}, {}'.format(id,time.localtime()))
    infos = conn.getSong(id)
    # print(infos['song']['size'])
    size = infos['song']['size']
    title = infos['song']['title']
    stream = conn.stream(id)
    # # print(type(stream))
    # print(stream.info())
    # b =''
    # # print(stream.read(amt=65536))
    newFile = open(title+'.mp3', "wb")
    # # write to file
    newFile.write(stream.read(amt=4755636))
    newFile.close()
    print('Fin {}, {}'.format(id,time.localtime()))