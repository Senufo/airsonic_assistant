
# Mémo Airsonic dev

## Environnement

1. Création environnement virtuel python:

`python3 -m venv /home/henri/Sources/airsonic`

2. Activation

`source bin/activate`

3. Installation des bibliothèques

- py-sonic: `pip install py-sonic`
- [Source sur github](https://github.com/crustymonkey/py-sonic)
- [Documentation](https://stuffivelearned.org/doku.php?id=programming:python:py-sonic)

4. Déactivation

`deactivate`

## Test py-sonic

1. lire une chanson avec l'id 12770 avec mplayer

curl https://myserver.airsonic.re/rest/stream.view\?u\=user\&p\=mypassword\&v\=1.15.0\&\&id\=12770\&c\=myapp | mplayer -   

2. Autentification avec token

[Voir docs ici](https://airsonic.github.io/docs/api/)

Pour générer le _salt_ de 10 caractères on peut utiliser une des commandes suivantes: 

```bash
openssl rand -base64 10
openssl rand -hex 10
head /dev/urandom | tr -dc A-Za-z0-9 | head -c 10 ; echo
```
Le token à utiliser avec la commande suivante:

```bash
echo -n <password><salt> | md5sum | awk '{ print $1 }'
```

Un test:

```bash
curl 'http://your-server/rest/ping.view?u=<username>&t=<token>&s=<salt>&v=1.15.0&c=<your-app>'
```